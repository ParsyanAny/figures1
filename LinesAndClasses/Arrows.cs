﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinesAndClasses
{
    class Arrows : Lines
    {
        public char Arrow { get; } = '>';
        public Arrows()
        {
            Arrow = '>';
        }
        public Arrows(byte length, char simbol)
        {
            Length1 = length;
            Simbol = simbol;
            Arrow = '>';
        }
        public Arrows(byte length)
        {
            Length1 = length;
            Simbol = '-';
            Arrow = '>';
        }
        public Arrows(char simbol)
        {
            Simbol = simbol;
            Length1 = 15;
            Arrow = '>';
        }
    }
}