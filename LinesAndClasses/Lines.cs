﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinesAndClasses
{
    class Lines : Figures
    {
        public Lines() { }
        public Lines(byte length, char simbol)
        {
            Length1 = length;
            Simbol = simbol;
        }
        public Lines(byte length) 
        {
            Length1 = length;
            Simbol = '-';
        }
        public Lines(char simbol)
        { 
            Simbol = simbol; 
            Length1 = 15;
        }      
    }    
}   