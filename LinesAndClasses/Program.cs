﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using static System.Console;

namespace LinesAndClasses
{
    class Program
    {
        static void Main(string[] args) 
        {
            //Lines line = new Lines(15);
            //Draw(line);

             Arrows arr = new Arrows(56,'-');
             Draw(arr);

           // Square sq = new Square(36,'-');
            //Draw(sq);

            //Rectangle rec = new Rectangle(25,35,'m');
            // Draw(rec);

            //Triangles tri = new Triangles(15,16,'-');
            // Draw(tri);

            Read();
        }
        static void Draw(Lines li)
        {
            if (li.Length1 > 0)
            {
                ForegroundColor = ConsoleColor.Red;
                SetCursorPosition(42, 12);
                WriteLine("This is your line!");
                ForegroundColor = ConsoleColor.Cyan;
                SetCursorPosition(42, 14);
                WriteLine($"Your line's length = {li.Length1}");
                ResetColor();
                //SetCursorPosition(60 - li.Length1/2, 16);
                for (int i = 0; i < li.Length1; i++)
                {
                    System.Threading.Thread.Sleep(55);
                    SetCursorPosition(60 - li.Length1 + i, 16);
                    Write(li.Simbol);
                }
                //WriteLine(new string(li.Simbol, li.Length1));
            }
        }
        static void Draw(Arrows arr)
        {
            if (arr.Length1 > 0)
            {
                ForegroundColor = ConsoleColor.Red;
                SetCursorPosition(46, 12);
                WriteLine("This is your arrow!!!");
                ForegroundColor = ConsoleColor.Cyan;
                SetCursorPosition(46, 14);
                WriteLine($"Your arrows's length = {arr.Length1}");
                ResetColor();
                for (int i = 0; i < arr.Length1; i++)
                {
                    System.Threading.Thread.Sleep(55);
                    SetCursorPosition(60 - arr.Length1 + i, 16);
                    Write(arr.Simbol);
                }
                Write(arr.Arrow);

                //SetCursorPosition(50, 15);
                //WriteLine(new string(arr.Simbol, arr.Length1) + arr.Arrow);
            }
        }
        static void Draw(Square sq)
        {
            if (sq.Length1 > 0)
            {
                ForegroundColor = ConsoleColor.Red;
                SetCursorPosition(46, 1);
                WriteLine("This is your square!");
                SetCursorPosition(46, 2);
                WriteLine($"Length of your square = {sq.Length1}");
                ForegroundColor = ConsoleColor.Green;
                SetCursorPosition(60 - sq.Length1 / 2, 9);
                WriteLine($"A = B = C = D = {sq.Length1}");
                ResetColor();
                for (int i = 0; i < sq.Length1; i++)
                {
                    SetCursorPosition(60 - sq.Length1 / 2 + i, 10);
                    Write(sq.Simbol);
                    SetCursorPosition(60 - sq.Length1 / 2 + i, 10 + sq.Length1 / 2);
                    Write(sq.Simbol);
                    System.Threading.Thread.Sleep(50);
                }
                for (int i = 0; i < sq.Length1 / 2; i++)
                {
                    SetCursorPosition(60 - sq.Length1 / 2, 10 + i);
                    Write(sq.Simbol);
                    SetCursorPosition(60 - sq.Length1 / 2 + sq.Length1 - 1, 10 + i);
                    Write(sq.Simbol);
                    System.Threading.Thread.Sleep(50);
                }
                ForegroundColor = ConsoleColor.Blue;
                SetCursorPosition(60, 12);
                WriteLine($"S = {4 * sq.Length1}");
                ForegroundColor = ConsoleColor.Cyan;
                SetCursorPosition(60, 11);
                WriteLine($"P {sq.Length1 + sq.Length1 + sq.Length1 + sq.Length1}");
            }
        }
        static void Draw(Rectangle rec)
        {
            if (rec.Length1 > 0 && rec.Length2 > 0)
            {
                WriteLine();
                ForegroundColor = ConsoleColor.Red;
                SetCursorPosition(47, 5);
                WriteLine("This is your rectangle!");
                SetCursorPosition(60 - rec.Length1 / 2, 9);
                WriteLine($"A1 = {rec.Length1}, A2 = {rec.Length2}");
                ResetColor();
                for (int i = 0; i < rec.Length1; i++)
                {
                    SetCursorPosition(60 - rec.Length1 / 2 + i, 10);
                    Write(rec.Simbol);
                    SetCursorPosition(60 - rec.Length1 / 2 + i, 10 + rec.Length2 / 2);
                    Write(rec.Simbol);
                    System.Threading.Thread.Sleep(50);
                }
                for (int i = 0; i < rec.Length2 / 2; i++)
                {
                    SetCursorPosition(60 - rec.Length1 / 2, 10 + i);
                    Write(rec.Simbol);
                    SetCursorPosition(60 - rec.Length1 / 2 + rec.Length1 - 1, 10 + i);
                    Write(rec.Simbol);
                    System.Threading.Thread.Sleep(50);
                }
                ForegroundColor = ConsoleColor.Blue;
                SetCursorPosition(60, 12);
                WriteLine($"S = {rec.Length1 * rec.Length2}");
                ForegroundColor = ConsoleColor.Cyan;
                SetCursorPosition(60, 13);
                WriteLine($"P = {rec.Length1 * 2 + rec.Length2 * 2}");
            }
        }
        static void Draw(Triangles tri)
        {
            if (tri.Length1 > 0 && tri.Length2 > 0)
            {
                ForegroundColor = ConsoleColor.Red;
                SetCursorPosition(46, 5);
                WriteLine("This is your triangle!");
                ResetColor();
                ForegroundColor = ConsoleColor.DarkYellow;
                SetCursorPosition(46, 7);
                WriteLine($"A = B = {tri.Length1}, C = {tri.Length2}");
                ResetColor();

                SetCursorPosition(60, 10);
                Write(tri.Simbol);
                for (int i = 0; i < tri.Length1; i++)
                {

                    SetCursorPosition(60 - i, 10 + i);
                    Write(tri.Simbol);
                    SetCursorPosition(60 + i, 10 + i);
                    Write(tri.Simbol);
                }
                SetCursorPosition(60 - tri.Length1, 10 + tri.Length1);

                for (int i = 0; i < tri.Length2; i++)
                {
                    Write(tri.Simbol + " ");
                }
                ForegroundColor = ConsoleColor.Cyan;
                SetCursorPosition(46, 9);
                WriteLine($"P = {tri.Length1 * 2 + tri.Length2}");
            }
        }
    }
}