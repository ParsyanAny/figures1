﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinesAndClasses
{
    class Rectangle : Figures
    {
        public Rectangle() { }
        public Rectangle(byte length1, byte length2, char simbol)
        {
            Length1 = length1;
            Length2 = length2;
            Simbol = simbol;
        }
        public Rectangle(byte length)
        {           
            Length1 = length;
            Length2 = 15;
            Simbol = '-';
        }
        public Rectangle(byte length, char simbol)
        {
            Length1 = length;
            Length2 = 15;
            Simbol = simbol;
        }       
        public Rectangle(char simbol)
        {
            Length1 = 15;
            Length2 = 15;
            Simbol = simbol;
        }
    }
}
