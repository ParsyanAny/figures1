﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinesAndClasses
{
    class Square : Figures
    {
        public Square() { }
        public Square(byte length, char simbol)
        {
            Length1 = length;
            Simbol = simbol;
        }
        public Square(byte length)
        {
            Length1 = length;
            Simbol = '-';
        }
        public Square(char simbol)
        {
            Length1 = 15;
            Simbol = simbol;
        }
    }
}