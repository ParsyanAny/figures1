﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace LinesAndClasses
{
    class Triangles : Rectangle
    {
        public Triangles() { }
        public Triangles(byte length1, byte length2, char simbol)
        {
            Length1 = length1;
            Length2 = length2;
            Simbol = simbol;
        }
        public Triangles(byte length)
        {
            Length1 = length;
            Length2 = 15;
            Simbol = '-';
        }
        public Triangles(byte length, char simbol)
        {
            Length1 = length;
            Length2 = 15;
            Simbol = simbol;
        }
        public Triangles(char simbol)
        {
            Length1 = 15;
            Length2 = 15;
            Simbol = simbol;
        }
    }
}